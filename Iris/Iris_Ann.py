# ------------------------------------------------------------------------------------------------------
# Imports
# ------------------------------------------------------------------------------------------------------

import torch
import torch.nn as nn
import torch.nn.functional as F
import pandas as pd
import numpy as np

from torch.utils.data import DataLoader
from sklearn.model_selection import train_test_split


# HyperParameters
features = 4
neurons_in_hidden_layer1 = 8
neurons_in_hidden_layer2 = 8
labels = 3

# ------------------------------------------------------------------------------------------------------
# Create a Feed Forward Network
# ------------------------------------------------------------------------------------------------------

class Iris_Ann(nn.Module):

    def __init__(self, in_feat, h1, h2, out_feat):
        super(Iris_Ann, self).__init__()

        self.fc1 = nn.Linear(in_feat, h1)
        self.fc2 = nn.Linear(h1, h2)
        self.out = nn.Linear(h2, out_feat)

    def forward(self, X):
        X = F.relu_(self.fc1(X))
        X = F.relu_(self.fc2(X))
        X = self.out(X)

        return X

# ------------------------------------------------------------------------------------------------------
#   Load Data
# ------------------------------------------------------------------------------------------------------

df = pd.read_csv('iris.csv')

X = df.drop('target', axis=1).values
y = df['target'].values

# Perform train/test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)

# print(X_train.dtype)
print(type(X_train))

# Convert the train/test split into tensor's
X_train = torch.FloatTensor(X_train)
X_test = torch.FloatTensor(X_test)

y_train = torch.LongTensor(y_train)
y_test = torch.LongTensor(y_test)

# shuffle the data and divide them into batches
train_loader = DataLoader(X_train, batch_size=60, shuffle=True)
test_loader = DataLoader(X_test, batch_size=60, shuffle=True)



# load_data()


# ------------------------------------------------------------------------------------------------------



# Initialize Network and define loss equations and optimizations

model = Iris_Ann(features, neurons_in_hidden_layer1, neurons_in_hidden_layer2, labels)
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=0.01)


# Train the model
def train_model():
    epochs = 100
    losses = []

    for i in range(epochs):

        i += 1
        y_pred = model.forward(X_train)
        loss = criterion(y_pred, y_train)
        losses.append(loss)

        if i%10 == 1:
            print(f'epoch: {i: 2} loss:{loss.item(): 10.8f}')

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()


def validate_model():
    correct = 0

    with torch.no_grad():

        for i, data in enumerate(X_test):

            y_val = model.forward(data)
            print(f'{i + 1:2} {str(y_val):38} {y_test[i]}')
            if y_val.argmax().item() == y_test[i]:
                correct += 1

    print(f'\n {correct} out of {len(y_test)} = {100 * correct / len(y_test): .2f}% correct')


train_model()

validate_model()




