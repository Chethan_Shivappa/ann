# README #

This Repo contains deep learning material, framework used is Pytorch.

### ANN : Artificial Neural Network ###

#### 1.Iris Dataset ####

* Quick summary
* The data set contains 3 classes of 50 instances each, where each class refers to a type of iris plant. One class is linearly separable from the other.
* [URL for iris dataset :](https://archive.ics.uci.edu/ml/datasets/iris)
